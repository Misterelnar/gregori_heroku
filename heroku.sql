﻿create table app(
app_code varchar(40) NOT NULL,
descrip varchar(40),
CONSTRAINT pk_app PRIMARY KEY (app_code)
);

create table record(
id serial NOT NULL,
app_code varchar(40),
player varchar(250),
score int,
datetime timestamp with time zone DEFAULT now(),
CONSTRAINT pk_records PRIMARY KEY (id),
CONSTRAINT fk_app FOREIGN KEY (app_code) REFERENCES app (app_code)
);
/*Insert app*/
insert into app (app_code,descrip) VALUES ('123ABC','Codig app ABC');
insert into app (app_code,descrip) VALUES ('456DEF','Codig app DEF');
insert into app (app_code,descrip) VALUES ('789GHI','Codig app GHI');
insert into app (app_code,descrip) VALUES ('101JKL','Codig app JKL');
insert into app (app_code,descrip) VALUES ('111MNO','Codig app MNO');

/*select * from app*/
/*Insert record*/
insert into record (app_code,player,score) VALUES ('123ABC','Gregori',923432);
insert into record (app_code,player,score) VALUES ('456DEF','Lluis',659524);
insert into record (app_code,player,score) VALUES ('789GHI','Ruben',834445);
insert into record (app_code,player,score) VALUES ('101JKL','KaNas',672834);
insert into record (app_code,player,score) VALUES ('111MNO','Toni',536755);

/*SELECT * FROM record WHERE app_code = '123ABC';*/
/*select * from record*/
/*
create table test (
T_id int,
T_name varchar(25)
);
insert into test (T_id,T_name) VALUES (1,'Gregor');*/

